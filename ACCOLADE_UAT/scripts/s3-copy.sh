#!/bin/bash
if [ -f /etc/profile.d/pipeline-asg.sh ];then
 source /etc/profile.d/pipeline-asg.sh
 if [[ ! -z "${SERVER_TYPE}" && ! -z "${PROJECT_DIR}" ]];then
    DATE=`date '+%Y-%m-%d-%H-%M-%S'`
    BACKUPNAME="${SERVER_TYPE}-$HOSTNAME-Date$DATE.tar.gz"
    /bin/rm -rf /tmp/logs/*
    /bin/mkdir -p /tmp/logs/ /tmp/logs/app-logs-backups/ /tmp/logs/nginx/ /tmp/logs/report /tmp/logs/mail/ /tmp/logs/auth/ /tmp/logs/syslog/
    /bin/cp -a /var/log/nginx/* /tmp/logs/nginx/
    /bin/cp -a ${PROJECT_DIR}current/codepool/var/log /tmp/logs/app-logs-backups/
    /bin/cp -a ${PROJECT_DIR}current/codepool/var/report /tmp/logs/report/
    /bin/cp -a /var/log/mail.log* /tmp/logs/mail/
    /bin/cp -a /var/log/auth.log* /tmp/logs/auth/
    /bin/cp -a /var/log/syslog* /tmp/logs/syslog/
    /bin/tar czf /tmp/${BACKUPNAME}  /tmp/logs >> /dev/null 2>&1
    /bin/mv /tmp/${BACKUPNAME} /apps3logs/
    /bin/rm -rf /tmp/logs/*/etc/profile
    echo "${BACKUPNAME} file"
 fi
fi