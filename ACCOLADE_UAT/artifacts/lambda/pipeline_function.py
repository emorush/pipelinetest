import json
import os
import boto3
import logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)

def spliteq(s):
    if '=' in s:
        [ key, value ] = s.split("=")
        return (key, value)
    return None

def trigger_action(event, context):
    if 'Records' in event and event['Records'][0]['Sns'] and event['Records'][0]['Sns']['Subject'] == "AWS CloudFormation Notification":

        valsList = list(filter(None,map(lambda s: spliteq(s), event['Records'][0]['Sns']['Message'].split("\n"))))
        valDict = dict(valsList)

        if valDict.get('LogicalResourceId') == "'CronServerGroup'" and valDict.get('ResourceStatus') == "'DELETE_COMPLETE'":
            client = boto3.client('ssm')
            stack = valDict.get('StackName','NOT_EXIST').strip("'")
            responseCC = client.send_command(DocumentName=os.environ['DOCUMENT'],
                Targets=[
                {
                    'Key': 'tag:Server Type',
                    'Values': [
                        stack+'-crons',
                    ]
                },
                ],Parameters={
                    'workingDirectory': [
                        os.environ['PROJECT_DIR']+'current/codepool'
                    ],
                    'executionTimeout': [
                        '120',
                    ]
                }, TimeoutSeconds=60,DocumentVersion='1')

            return stack+' --> CacheClean Executed'
