<?php
return [
    'modules' => [
    ],
    'scopes' => [
        'websites' => [
            'admin' => [
                'website_id' => '0',
                'code' => 'admin',
                'name' => 'Admin',
                'sort_order' => '0',
                'default_group_id' => '0',
                'is_default' => '0',
                'wcode' => NULL,
                'plant' => NULL,
                'warehouse' => NULL,
            ],
            'au_website' => [
                'website_id' => '1',
                'code' => 'au_website',
                'name' => 'AU Website',
                'sort_order' => '0',
                'default_group_id' => '1',
                'is_default' => '1',
                'wcode' => NULL,
                'plant' => NULL,
                'warehouse' => NULL,
            ],
            'uk_website' => [
                'website_id' => '2',
                'code' => 'uk_website',
                'name' => 'UK Website',
                'sort_order' => '1',
                'default_group_id' => '2',
                'is_default' => '0',
                'wcode' => NULL,
                'plant' => NULL,
                'warehouse' => NULL,
            ],
        ],
        'groups' => [
            0 => [
                'group_id' => '0',
                'website_id' => '0',
                'code' => 'default',
                'name' => 'Default',
                'root_category_id' => '0',
                'default_store_id' => '0',
            ],
            1 => [
                'group_id' => '1',
                'website_id' => '1',
                'code' => 'au_store',
                'name' => 'AU Store',
                'root_category_id' => '2',
                'default_store_id' => '1',
            ],
            2 => [
                'group_id' => '2',
                'website_id' => '2',
                'code' => 'uk_store',
                'name' => 'UK Store',
                'root_category_id' => '2',
                'default_store_id' => '2',
            ],
        ],
        'stores' => [
            'admin' => [
                'store_id' => '0',
                'code' => 'admin',
                'website_id' => '0',
                'group_id' => '0',
                'name' => 'Admin',
                'sort_order' => '0',
                'is_active' => '1',
            ],
            'au_storeview' => [
                'store_id' => '1',
                'code' => 'au_storeview',
                'website_id' => '1',
                'group_id' => '1',
                'name' => 'AU Storeview',
                'sort_order' => '0',
                'is_active' => '1',
            ],
            'uk_storeview' => [
                'store_id' => '2',
                'code' => 'uk_storeview',
                'website_id' => '2',
                'group_id' => '2',
                'name' => 'UK Storeview',
                'sort_order' => '1',
                'is_active' => '1',
            ],
        ],
    ],
    'themes' => [
        'frontend/Magento/blank' => [
            'parent_id' => NULL,
            'theme_path' => 'Magento/blank',
            'theme_title' => 'Magento Blank',
            'is_featured' => '0',
            'area' => 'frontend',
            'type' => '0',
            'code' => 'Magento/blank',
        ],
        'adminhtml/Magento/backend' => [
            'parent_id' => NULL,
            'theme_path' => 'Magento/backend',
            'theme_title' => 'Magento 2 backend',
            'is_featured' => '0',
            'area' => 'adminhtml',
            'type' => '0',
            'code' => 'Magento/backend',
        ],
        'frontend/Magento/luma' => [
            'parent_id' => 'Magento/blank',
            'theme_path' => 'Magento/luma',
            'theme_title' => 'Magento Luma',
            'is_featured' => '0',
            'area' => 'frontend',
            'type' => '0',
            'code' => 'Magento/luma',
        ],
        'frontend/Netstarter/eagle' => [
            'parent_id' => 'Magento/blank',
            'theme_path' => 'Netstarter/eagle',
            'theme_title' => 'Netstarter Eagle',
            'is_featured' => '0',
            'area' => 'frontend',
            'type' => '0',
            'code' => 'Netstarter/eagle',
        ],
        'frontend/AW/cellarone' => [
            'parent_id' => 'Netstarter/eagle',
            'theme_path' => 'AW/cellarone',
            'theme_title' => 'CellarOne',
            'is_featured' => '0',
            'area' => 'frontend',
            'type' => '0',
            'code' => 'AW/cellarone',
        ],
        'frontend/AW/cellarone_uk' => [
            'parent_id' => 'AW/cellarone',
            'theme_path' => 'AW/cellarone_uk',
            'theme_title' => 'CellarOne UK',
            'is_featured' => '0',
            'area' => 'frontend',
            'type' => '0',
            'code' => 'AW/cellarone_uk',
        ],
    ],
    'system' => [
        'default' => [
            'dev' => [
                'debug' => [
                    'profiler' => '0',
                    'template_hints_storefront' => '0',
                    'template_hints_admin' => '0',
                    'template_hints_blocks' => '0',
                    'debug_logging' => '0',
                ],
                'js' => [
                    'merge_files' => '1',
                    'minify_files' => '1',
                    'minify_exclude' => '/tiny_mce/
                    /connect.facebook.net/
                    /api.cloudsponge.com/
        ',
                    'session_storage_logging' => '0',
                    'translate_strategy' => 'dictionary',
                    'enable_js_bundling' => '1',
                ],
                'css' => [
                    'minify_files' => '1',
                    'minify_exclude' => '
                    /tiny_mce/
                ',
                    'merge_css_files' => '1',
                ],
                'image' => [
                    'default_adapter' => 'GD2',
                    'adapters' => [
                        'GD2' => [
                            'title' => 'PHP GD2',
                            'class' => 'Magento\\Framework\\Image\\Adapter\\Gd2',
                        ],
                        'IMAGEMAGICK' => [
                            'title' => 'ImageMagick',
                            'class' => 'Magento\\Framework\\Image\\Adapter\\ImageMagick',
                        ],
                    ],
                ],
                'static' => [
                    'sign' => '1',
                ],
                'template' => [
                    'minify_html' => '1',
                    'allow_symlink' => '0',
                ],
                'grid' => [
                    'async_indexing' => '1',
                ],
                'front_end_development_workflow' => [
                    'type' => 'server_side_compilation',
                ],
                'translate_inline' => [
                    'active' => '0',
                    'active_admin' => '0',
                    'invalid_caches' => [
                        'block_html' => NULL,
                    ],
                ],
            ],
        ],
    ],
];
