#!/bin/bash
PROJECT_DIR=$1

if [ -d ${PROJECT_DIR}current/ ];then
    rm -rf ${PROJECT_DIR}current
fi

aws configure set s3.signature_version s3v4
aws s3 cp $(sed 's|arn:aws:s3:::|s3://|g' /root/.deploy/current_code_version) buildzip
mkdir -p ${PROJECT_DIR}current/
unzip buildzip
tar mxfz codepool.tar.gz -C ${PROJECT_DIR}current/

cd ${PROJECT_DIR}current/codepool/
mkdir -p var/report
chown -R deploy:www-data .
find generated app/etc -type f -exec chmod g+w {} \;
find generated app/etc var -type d -exec chmod g+ws {} \;
find app/code lib app/etc generated/metadata \( -type d -or -type f \) -exec chmod g-w {} \;
chmod o-rwx app/etc/env.php

rm -rf pub/media pub/sitemap.xml pub/static/_cache pub/robots.txt
sudo -u deploy ln -s  ${PROJECT_DIR}shared/codepool/pub/media pub
sudo -u deploy ln -s  ${PROJECT_DIR}shared/codepool/var/export var
sudo -u deploy ln -s  ${PROJECT_DIR}shared/codepool/pub/static/_cache pub/static
sudo -u deploy ln -s  ${PROJECT_DIR}shared/codepool/pub/robots.txt pub/robots.txt
sudo -u deploy ln -s  ${PROJECT_DIR}shared/codepool/var/dotmailer var


