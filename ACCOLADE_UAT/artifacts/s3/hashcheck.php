<?php
/**
 * Created by PhpStorm.
 * User: pmk
 * Date: 7/09/18
 * Time: 2:14 PM
 */
$configFiles = ['env.php', 'config.php'];
$requiredConfig = ['scopes', 'themes', 'system'];
$configs = [];
foreach ($configFiles as $configFile) {
    if (file_exists($configFile)) {
        $fileData = include $configFile;
    } else {
        continue;
    }
    $allFilesData[$configFile] = $fileData;
    if (is_array($fileData) && count($fileData) > 0) {
        $configs = array_replace_recursive($configs, $fileData);
    }
}

$allHashes = [];
foreach ($configs as $section => $config) {
    if (null === $config || !in_array($section, $requiredConfig)) {
        continue;
    }
    echo "section: -->".$section."\n";
    echo "json: -->".json_encode($config)."\n";
    echo "hash: -->".sha1(json_encode($config))."\n\n";
    $allHashes[$section] = sha1(json_encode($config));
}
echo "-----------------------------------------------\n\n";
echo "All Hash -->".json_encode($allHashes)."\n\n";