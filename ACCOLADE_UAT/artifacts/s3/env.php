<?php
return [
    'backend' => [
        'frontName' => 'admin'
    ],
    'db' => [
        'connection' => [
            'indexer' => [
                'host' => 'awm2-uat-db.cnbu3gydne0u.ap-southeast-2.rds.amazonaws.com',
                'dbname' => 'awm2uatdb12',
                'username' => 'accoladeuat',
                'password' => 'dde3wx5rDs',
                'active' => '1',
                'persistent' => NULL
            ],
            'default' => [
                'host' => 'awm2-uat-db.cnbu3gydne0u.ap-southeast-2.rds.amazonaws.com',
                'dbname' => 'awm2uatdb12',
                'username' => 'accoladeuat',
                'password' => 'dde3wx5rDs',
                'active' => '1'
            ]
        ],
        'table_prefix' => ''
    ],
    'crypt' => [
        'key' => '063dc466d5bf163a67731f4ae3544139'
    ],
    'resource' => [
        'default_setup' => [
            'connection' => 'default'
        ]
    ],
    'cache' => [
        'frontend' => [
            'default' => [
                'backend' => 'Cm_Cache_Backend_Redis',
                'backend_options' => [
                    'server' => 'awm2-cache.dr0cim.ng.0001.apse2.cache.amazonaws.com',
                    'database' => '0',
                    'port' => '6379'
                ]
            ]
        ]
    ],
    'x-frame-options' => 'SAMEORIGIN',
    'MAGE_MODE' => 'production',
    'session' => [
        'save' => 'redis',
        'redis' => [
            'host' => 'awm2-session.dr0cim.ng.0001.apse2.cache.amazonaws.com',
            'port' => '6379',
            'password' => '',
            'timeout' => '2.5',
            'persistent_identifier' => '',
            'database' => '2',
            'compression_threshold' => '2048',
            'compression_library' => 'gzip',
            'log_level' => '1',
            'max_concurrency' => '6',
            'break_after_frontend' => '5',
            'break_after_adminhtml' => '30',
            'first_lifetime' => '600',
            'bot_first_lifetime' => '60',
            'bot_lifetime' => '7200',
            'disable_locking' => '0',
            'min_lifetime' => '60',
            'max_lifetime' => '2592000'
        ]
    ],
    'cache_types' => [
        'config' => 1,
        'layout' => 1,
        'block_html' => 1,
        'collections' => 1,
        'reflection' => 1,
        'db_ddl' => 1,
        'eav' => 1,
        'customer_notification' => 1,
        'config_integration' => 1,
        'config_integration_api' => 1,
        'target_rule' => 1,
        'full_page' => 1,
        'translate' => 1,
        'config_webservice' => 1,
        'compiled_config' => 1,
        'store_locator' => 1
    ],
    'install' => [
        'date' => 'Tue, 13 Mar 2018 07:03:46 +0000'
    ],
    'http_cache_hosts' => [
        [
            'host' => '10.0.2.109',
            'port' => '6081'
        ]
    ],
    'system' => [
        'default' => [
            'dev' => [
                'debug' => [
                    'debug_logging' => '0'
                ]
            ]
        ]
    ]
];
